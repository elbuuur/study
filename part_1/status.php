<?php
session_start();
include 'status_functions.php';

$editUserId = (int)$_GET['id'];
$loggedUserId = (int)$_SESSION['id'];

if($_POST['id']) {
    $inputId = $_POST['id'];
}

if($_POST['status']) {
    $status = $_POST['status'];
}

/* проверяем, авторизован ли пользователь */
if(empty($loggedUserId)){
    redirect_to('page_login.php');
    die();
}

/* проверяем автор ли текущий пользователь */
if (isAuthor($loggedUserId, $editUserId) == false){
    set_flash_message('warning', 'Редактировать можно только свой профиль');
    redirect_to('users.php');
    die();
}

$userInfo = getUserById($editUserId); // получили информацию для инпутов

if($status) {
    editStatus($inputId, $status);
    set_flash_message('success', 'Статус успешно обновлен');
    redirect_to('status_page.php?id='.$inputId);
}
