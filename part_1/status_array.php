<?php
/*массив со значениями статусов*/
$statusesArr = [
    [
        'name' => 'danger',
        'value' => 'Не беспокоить'
    ],
    [
        'name' => 'warning',
        'value' => 'Отошел'
    ],
    [
        'name' => 'success',
        'value' => 'Онлайн'
    ]
];
