<?php

use function Sodium\add;

session_start();

include 'function.php';

$email = $_POST['email'];
$password = $_POST['password'];
$name = $_POST['name'];
$position = $_POST['position'];
$phone = $_POST['phone'];
$address = $_POST['address'];
$file = $_POST['file'];
$vk = $_POST['vk'];
$telegram = $_POST['telegram'];
$instagram = $_POST['instagram'];
$status = $_POST['status'];
$img = $_FILES['file'];

if (!isAdmin($_SESSION['role']) || (empty($_SESSION['user']))) //если не админ и не авторизован, редиректим на страницу авторизации
{
    redirect_to('page_login.php');
    exit();
}

if (!empty(get_user_by_email($email))) //проверка на доступность email. если занят, редирект на ту же страницу и вывод сообщения
{
    set_flash_message('danger', 'Данный e-mail занят. Попробуйте другой.');
    redirect_to('create_user.php');
    exit();
}

$userId = add_user($email, $password);

if ($userId) {
    editUser($userId, $name, $position, $phone, $address); //добавили общие свойства

    setStatus($userId, $status); // установили статус

    if ($img) {
        downloadAvatar($userId, $img, 'img/'); //загрузили аватар
    }

    addSocialLinks($userId, $vk, $telegram, $instagram); //заливаем ссылки на соцсети

    set_flash_message('success', 'Пользователь создан. Ура!');
    redirect_to('users.php');
    exit();
}