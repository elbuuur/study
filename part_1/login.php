<?php
session_start();
/*
 * подключили файл со всеми функциями
 */
include "function.php";

$email = $_POST['email'];
$password = $_POST['password'];

$user = login($email, $password);

isNotLogged($user, 'danger', 'Пользователь не найден', 'page_login.php');

redirect_to('users.php');
