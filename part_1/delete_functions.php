<?php

/**
 *@param - $path
 * Description: редирект на определенную страницу
 * Return value: null
 **/
function redirect_to($path)
{
    header('Location: ' . $path);
}

/**
 * @param - $name (ключ)
 * @param string - $message
 * Description: подготовить флеш-сообщение
 * Retutn value: null
 **/
function set_flash_message($name, $message)
{
    $_SESSION[$name] = $message;
}

/**
 *@param string - $name (ключ)
 *Description: вывести флеш-сообщение
 * Return value: null
 **/
function display_flash_message($name)
{
    if (isset($_SESSION[$name])): ?>
        <div class="alert alert-<?= $name; ?>" role="alert">
            <?php echo $_SESSION[$name];
            unset($_SESSION[$name]);
            ?>
        </div>
    <? endif;
}

/*
 * @param $loggedUserId - int (id автор-го пользователя)
 * @param $editUserId - int (id редактируемого пол-ля)
 * Description: проверить автор ли текущий пользователь
 * Return value: boolean
 */
function isAuthor($loggedUserId, $editUserId) {
    if($_SESSION['role'] == 'admin') {
        return true;
    } elseif($loggedUserId == $editUserId) {
        return true;
    } else {
        return false;
    }
}

/*
 * @param $role - string
 * descr: проверка на админа
 * return value: boolean
 */

function isAdmin($role)
{
    $role = trim($role);
    if ($role == 'admin') {
        return true;
    }
}

/*
 * @param $userId - string (id пользователя из бд, получен из функции add_user)
 * @param $name - string
 * @param $position - string
 * @param $phone - string
 * @param $address - string
 * descr: добавляет свойства новому пользователю
 */
function deleteUser($userId)
{
    $pdo = new PDO('mysql:host=localhost;dbname=part_1', 'root', 'root');
    $sql = "DELETE FROM users WHERE id = :id";
    $statement = $pdo->prepare($sql);
    $statement->execute(['id' => $userId]);
}