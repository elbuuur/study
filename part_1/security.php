<?php
session_start();
include 'security_functions.php';

$editUserId = (int)$_GET['id'];
$loggedUserId = (int)$_SESSION['id'];

/* получаем данные с инпутов */
if($_POST['email']) {
    $email = $_POST['email'];
}
if($_POST['password']) {
    $password = $_POST['password'];
}

if($_POST['now-email']) {
    $nowEmail = $_POST['now-email'];
}

if($_POST['id']) {
    $inputId = $_POST['id'];
}

/* проверяем, авторизован ли пользователь */
if(empty($loggedUserId)){
    redirect_to('page_login.php');
    die();
}

/* проверяем автор ли текущий пользователь */
if (isAuthor($loggedUserId, $editUserId) == false){
    set_flash_message('warning', 'Редактировать можно только свой профиль');
    redirect_to('users.php');
    die();
}

$userInfo = getUserById($editUserId); // получили информацию для инпутов

if($email != $nowEmail && getUserByEmail($email)) { //если данная почта уже есть в бд или если эта рабочая почта пользователя
    set_flash_message('danger', 'Данный e-mail занят. Попробуйте другой.');
    redirect_to($_SERVER['HTTP_REFERER']);
    die();
} elseif($email != $nowEmail) {
    $editedUser = editUser($inputId, $email, $password);
    set_flash_message('success', 'Профиль успешно обновлен');
    redirect_to('security_page.php?id='.$inputId);
    die();
}