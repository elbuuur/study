<?php
session_start();
include 'profile_function.php';

$loggedUser = $_SESSION['id'];

if(empty($loggedUser)) {
    redirect_to('login.php');
    die();
}

$userId = (int)$_GET['id'];

$user = getUserId($userId);