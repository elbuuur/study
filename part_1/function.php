<?php

use http\Message;

session_start();

/**
 * @param string - $email
 * Description: осущ-т поиск пользователя по e-mail
 * Return value: array
 **/
function get_user_by_email($email)
{
    $pdo = new PDO('mysql:host=localhost;dbname=part_1;', 'root', 'root');
    $sql = 'SELECT * FROM users WHERE email=:email';
    $statement = $pdo->prepare($sql);
    $statement->execute(['email' => $email]);
    $user = $statement->fetch(PDO::FETCH_ASSOC);
    return $user;
}

/**
 * @param string - $email
 * @param string - $password
 * Description: добавляет пользователя в бд
 * Return value: int (user_id)
 **/
function add_user($email, $password)
{
    $pdo = new PDO('mysql:host=localhost;dbname=part_1', 'root', 'root');
    $sql = 'INSERT INTO users (email, password) VALUES (:email, :password)';
    $statement = $pdo->prepare($sql);
    $statement->execute(['email' => $email, 'password' => password_hash($password, PASSWORD_DEFAULT)]);

    return $pdo->lastInsertId();
}

/**
 * @param - $name (ключ)
 * @param string - $message
 * Description: подготовить флеш-сообщение
 * Retutn value: null
 **/
function set_flash_message($name, $message)
{
    $_SESSION[$name] = $message;
}

/**
 *@param string - $name (ключ)
 *Description: вывести флеш-сообщение
 * Return value: null
 **/
function display_flash_message($name)
{
    if (isset($_SESSION[$name])): ?>
        <div class="alert alert-<?= $name; ?>" role="alert">
            <?php echo $_SESSION[$name];
            unset($_SESSION[$name]);
            ?>
        </div>
    <? endif;
}

/**
 *@param - $path
 * Description: редирект на определенную страницу
 * Return value: null
 **/
function redirect_to($path)
{
    header('Location: ' . $path);
}

/*
 * @param - $email
 * @param - $password
 * Description: авторизация пользователя
 * Return value: boolean
 */

function login($email, $password)
{
    $pdo = new PDO('mysql:host=localhost;dbname=part_1;', 'root', 'root');
    $sql = 'SELECT * FROM users WHERE email=:email';
    $statement = $pdo->prepare($sql);
    $statement->execute(['email' => $email]);
    $user = $statement->fetch(PDO::FETCH_ASSOC);

    if ($user) {
        if (password_verify($password, $user['password'])) {
            $_SESSION['id'] = $user['id'];
            $_SESSION['user'] = $user['email'];
            $_SESSION['role'] = $user['role'];
            return true;
        } else {
            return false;
        }
    }
}

/*
 * @param $user - boolean
 * @param $path - string (страница, на которую будет происходить редирект)
 * description: если не авторизован, редиректит на другую страницу
 */
function isNotLogged($user, $name, $message, $path)
{
    if (!$user) {
        set_flash_message($name, $message);
        redirect_to($path);
        exit();
    }
}

/*
 * @param $role - string
 * descr: проверка на админа
 * return value: boolean
 */

function isAdmin($role)
{
    $role = trim($role);
    if ($role == 'admin') {
        return true;
    }
}

/*
 * @param $phone - string
 * descr: удаление из номера тире и пробелов для атрибута href
 * return value: 'очищенный' номер
 */
function changePhone($phone)
{
    $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
    return $phone;
}

/*
 * @param $userId - string (id пользователя из бд, получен из функции add_user)
 * @param $name - string
 * @param $position - string
 * @param $phone - string
 * @param $address - string
 * descr: добавляет свойства новому пользователю
 */
function editUser($userId, $name, $position, $phone, $address)
{
    $pdo = new PDO('mysql:host=localhost;dbname=part_1', 'root', 'root');
    $sql = "UPDATE users SET name = :name, position = :position, phone = :phone, address = :address WHERE id = :userid";
    $statement = $pdo->prepare($sql);
    $statement->execute(
        [
            'userid' => $userId,
            'name' => $name,
            'position' => $position,
            'phone' => $phone,
            'address' => $address
        ]
    );
}

/*
 * @param $userId - string (id пользователя из бд, получен из функции add_user)
 * @param $status - string (статус пользователя)
 * descr: устанавливает статус
 */

function setStatus($userId, $status)
{
    $pdo = new PDO('mysql:host=localhost;dbname=part_1', 'root', 'root');
    $sql = "UPDATE users SET status = :status WHERE id = :userid";
    $statement = $pdo->prepare($sql);
    $statement->execute(
        [
            'userid' => $userId,
            'status' => $status
        ]
    );
}

/*
 * @param $userId - string (id пользователя из бд, получен из функции add_user)
 * @param $file - array (массив с файлом)
 * @param $uploaddir - string (путь, куда сохраняется файл)
 * descr: заливает аватар пользователя на сервер
 */

function downloadAvatar($userId, $file, $uploaddir) {
    $uploadfile = $uploaddir . basename($file['name']);
    $filePlace = move_uploaded_file($file['tmp_name'], $uploadfile);

    if ($filePlace) {
        $pdo = new PDO('mysql:host=localhost;dbname=part_1', 'root', 'root');
        $sql = "UPDATE users SET img = :img WHERE id = :userid";
        $statement = $pdo->prepare($sql);
        $statement->execute(
            [
                'userid' => $userId,
                'img' => $uploadfile
            ]
        );
    }
}

/*
 * @param $userId - string (id пользователя из бд, получен из функции add_user)
 * @param $vk - string
 * @param $telegram - string
 * @param $instagram - string
 * descr: заливает ссылки на соцсети
 */

function addSocialLinks($userId, $vk, $telegram, $instagram) {
    $pdo = new PDO('mysql:host=localhost;dbname=part_1', 'root', 'root');
    $sql = "UPDATE users SET vk = :vk, telegram = :telegram, instagram = :instagram WHERE id = :userid";
    $statement = $pdo->prepare($sql);
    $statement->execute(
        [
            'userid' => $userId,
            'vk' => $vk,
            'telegram' => $telegram,
            'instagram' => $instagram
        ]
    );
}