<?php
session_start();
include 'edit_functions.php';

$editUserId = (int)$_GET['id'];

$loggedUserId = (int)$_SESSION['id'];

/* получаем данные с инпутов */
$name = $_POST['name'];
$position = $_POST['position'];
$phone = $_POST['phone'];
$address = $_POST['address'];
$inputId = $_POST['id'];

/* проверяем автор ли текущий пользователь */
if(empty($loggedUserId)){
    redirect_to('page_login.php');
    die();
}

if (isAuthor($loggedUserId, $editUserId) == false){
    set_flash_message('warning', 'Редактировать можно только свой профиль');
    redirect_to('users.php');
    die();
}

$userInfo = getUserById($editUserId); // получили информацию для инпутов

if(isset($name) || isset($position) || isset($phone) || isset($address) || isset($inputId)) {
    editUser($inputId, $name, $position, $phone, $address); //добавили общие свойства
    set_flash_message('success', 'Профиль успешно обновлен');
    redirect_to('users.php');
}