<?php
session_start();
include 'delete_functions.php';

$editUserId = (int)$_GET['id'];
$loggedUserId = (int)$_SESSION['id'];

/* проверяем, авторизован ли пользователь */
if(empty($loggedUserId)){
    redirect_to('page_login.php');
    die();
}

/* проверяем автор ли текущий пользователь */
if (isAuthor($loggedUserId, $editUserId) == false){
    set_flash_message('warning', 'Удалять можно только свой профиль');
    redirect_to('users.php');
    die();
}

$isAdmin = isAdmin($_SESSION['role']);

if($isAdmin) {
    deleteUser($editUserId);
    set_flash_message('success', 'Пользователь с id '.$editUserId.' удален');
    redirect_to('users.php');
    die();
} else {
    deleteUser($editUserId);
    session_destroy();
    redirect_to('login.php');
    die();
}