<?php

/**
 * @param string - $userId
 * Description: осущ-т поиск пользователя по id
 * Return value: array
 **/
function getUserId($userId)
{
    $pdo = new PDO('mysql:host=localhost;dbname=part_1;', 'root', 'root');
    $sql = 'SELECT * FROM users WHERE id=:id';
    $statement = $pdo->prepare($sql);
    $statement->execute(['id' => $userId]);
    $user = $statement->fetch(PDO::FETCH_ASSOC);
    return $user;
}

/*
 * @param $phone - string
 * descr: удаление из номера тире и пробелов для атрибута href
 * return value: 'очищенный' номер
 */
function changePhone($phone)
{
    $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
    return $phone;
}

/**
 *@param - $path
 * Description: редирект на определенную страницу
 * Return value: null
 **/
function redirect_to($path)
{
    header('Location: ' . $path);
}