<?php
session_start();
include 'media_functions.php';

$editUserId = (int)$_GET['id'];
$loggedUserId = (int)$_SESSION['id'];

if($_POST['id']) {
    $inputId = $_POST['id'];
}

if($_FILES['file']) {
    $img = $_FILES['file'];
}

/* проверяем, авторизован ли пользователь */
if(empty($loggedUserId)){
    redirect_to('page_login.php');
    die();
}

/* проверяем автор ли текущий пользователь */
if (isAuthor($loggedUserId, $editUserId) == false){
    set_flash_message('warning', 'Редактировать можно только свой профиль');
    redirect_to('users.php');
    die();
}

$userInfo = getUserById($editUserId); // получили информацию для инпутов

if($img) {
    downloadAvatar($inputId, $img, 'img/');
    set_flash_message('success', 'Аватар обновлен');
    redirect_to('page_profile.php?id='.$inputId);
}
