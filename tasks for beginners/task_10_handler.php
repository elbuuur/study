<?php
session_start();

$text = $_POST['text'];

$pdo = new PDO('mysql:host=localhost;dbname=my_project;', 'root', 'root');

$sql = 'SELECT * FROM task_9 WHERE text=:text';
$statement = $pdo ->prepare($sql);
$statement ->execute(['text' => $text]);
$task = $statement -> fetch(PDO::FETCH_ASSOC);

if(!empty($task)) {
    $message = 'Введеная запись уже присутствует в таблице';
    $_SESSION['danger'] = $message;
    header('Location: /task_10.php');
    exit;
}

$sql = 'INSERT INTO task_9 (text) VALUES (:text)';
$statement = $pdo -> prepare($sql);
$statement -> execute(['text' => $text]);

$message = 'Запись добавлена в таблицу';
$_SESSION['success'] = $message;

header('Location: /task_10.php');