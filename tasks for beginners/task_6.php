<?
$peoples = [
    [
        'photo' => 'img/demo/authors/sunny.png',
        'name' => 'Sunny A.',
        'position' => 'UI/UX Expert',
        'descr' => 'Lead Author',

        'socials' => [
            'twitter_title' => '@myplaneticket',
            'twitter_href' => 'https://twitter.com/@myplaneticket',
            'mail_title' => 'Contact Sunny',
            'mail_href' => 'https://wrapbootstrap.com/user/myorange',
        ],
        'banned' => false,
    ],
    [
        'photo' => 'img/demo/authors/josh.png',
        'name' => 'Jos K.',
        'position' => 'ASP.NET Developer',
        'descr' => 'Partner & Contributor',
        'socials' => [
            'twitter_title' => '@atlantez',
            'twitter_href' => 'https://twitter.com/@atlantez',
            'mail_title' => 'Contact Jos',
            'mail_href' => 'https://wrapbootstrap.com/user/Walapa',
        ],
        'banned' => false
    ],
    [
        'photo' => 'img/demo/authors/jovanni.png',
        'name' => 'Jovanni L.',
        'position' => 'PHP Developer',
        'descr' => 'Partner & Contributor',
        'socials' => [
            'twitter_title' => '@lodev09',
            'twitter_href' => 'https://twitter.com/@lodev09',
            'mail_title' => 'Contact Jovanni',
            'mail_href' => 'https://wrapbootstrap.com/user/lodev09',
        ],
        'banned' => true
    ],
    [
        'photo' => 'img/demo/authors/roberto.png',
        'name' => 'Roberto R.',
        'position' => 'Rails Developer',
        'descr' => 'Partner & Contributor',
        'socials' => [
            'twitter_title' => '@sildur',
            'twitter_href' => 'https://twitter.com/@sildur',
            'mail_title' => 'Contact Roberto',
            'mail_href' => 'https://wrapbootstrap.com/user/sildur',
        ],
        'banned' => true
    ]
]
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>
        Подготовительные задания к курсу
    </title>
    <meta name="description" content="Chartist.html">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
    <link id="vendorsbundle" rel="stylesheet" media="screen, print" href="css/vendors.bundle.css">
    <link id="appbundle" rel="stylesheet" media="screen, print" href="css/app.bundle.css">
    <link id="myskin" rel="stylesheet" media="screen, print" href="css/skins/skin-master.css">
    <link rel="stylesheet" media="screen, print" href="css/statistics/chartist/chartist.css">
    <link rel="stylesheet" media="screen, print" href="css/miscellaneous/lightgallery/lightgallery.bundle.css">
    <link rel="stylesheet" media="screen, print" href="css/fa-solid.css">
    <link rel="stylesheet" media="screen, print" href="css/fa-brands.css">
    <link rel="stylesheet" media="screen, print" href="css/fa-regular.css">
</head>
<body class="mod-bg-1 mod-nav-link ">
<main id="js-page-content" role="main" class="page-content">
    <div class="col-md-6">
        <div id="panel-1" class="panel">
            <div class="panel-hdr">
                <h2>
                    Задание
                </h2>
                <div class="panel-toolbar">
                    <button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
                    <button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
                </div>
            </div>
            <div class="panel-container show">
                <div class="panel-content">
                    <div class="d-flex flex-wrap demo demo-h-spacing mt-3 mb-3">
                        <? foreach ($peoples as $people): ?>
                            <div class=" <? if($people['banned']){ ?> banned <?}?> rounded-pill bg-white shadow-sm p-2 border-faded mr-3 d-flex flex-row align-items-center justify-content-center flex-shrink-0">
                                <img src="<?= $people['photo']; ?>" alt="<?= $people['name']; ?>" class="img-thumbnail img-responsive rounded-circle" style="width:5rem; height:5rem;">
                                <div class="ml-2 mr-3">
                                    <h5 class="m-0">
                                        <?= $people['name']; ?> (<?= $people['position']; ?>)
                                        <small class="m-0 fw-300">
                                            <?= $people['descr']; ?>
                                        </small>
                                    </h5>
                                    <a href="<?= $people['socials']['twitter_href']; ?>" class="text-info fs-sm" target="_blank"><?= $people['socials']['twitter_title']; ?></a> -
                                    <a href="<?= $people['socials']['mail_href']; ?>" class="text-info fs-sm" target="_blank" title="<?= $people['socials']['mail_title']; ?>"><i class="fal fa-envelope"></i></a>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>
<script>
    // default list filter
    initApp.listFilter($('#js_default_list'), $('#js_default_list_filter'));
    // custom response message
    initApp.listFilter($('#js-list-msg'), $('#js-list-msg-filter'));
</script>
</body>
</html>
